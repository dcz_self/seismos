# Seismos

A minimal-overhead file system for long logs on tiny devices.

## "Seismos" = "quake"

- because it appends data, like a seismograph on a sheet of rolled paper,
- because it was made to record accelerometer readings,
- because at some point, it will need to handle flaky connections, like an SD card pins disconnecting under shocks.

## Design goals

- Copy-on-write
- Log-structured
- No occupancy lists; determined probabilistically
- Static wear leveling (dynamic can be tacked on)
- Suitable for NAND and NOR flash
- Error correction (TODO)
- Arbitrary write or erase block sizes (erase must be at least as big as write)
- Store long append-only streams with low overhead
- Store short key-value pairs (TODO)
- Bounded space complexity (linear with open file handles)
- Bad blocks (TODO)

## Semantics

The file structure is flat. File names are 32-bit integers.
A file can have up to 65536 *sheet*s, each representing a separate log. 

There's only one write mode: "append". Every time the file is opened for appending, a new *sheet* is created with a consecutive number.

A sheet is flushed on close.

Sheets can be read block by block. Seismos does not store the exact sheet length (TODO?), so make sure the data stored is self-describing. The last block read out will contain at least the end of the *sheet* (even if it's empty).

To determine how many sheets exist in a file, try reading them starting with sheet 0. The first one which doesn't contain any blocks (even empty ones) is nonexistent. If sheet 0 doesn't exist, the file doesn't.

### Key-value pairs (TODO)

Small strings can be saved, read, and updated atomically using a separate API.

### Deletion

Seismos is a COW, long-structured file system. Each operation causes a write in a new place.

This means that it's possible that a deletion fails because there is no space. Just keep trying, and you'll eventually find a file that can actually be deleted (TODO: not a a concern yet).

### TODO: Preview version

Currently, Seismos can only handle 1 file at a time. What hasn't been tested:

- Anything that involves more than 1 file
- 2 open sheets at a time
- Deletion when a sheet is open (this will rather certainly break things)

## Design

Seismos accepts storage devices with separate erase and write block sizes.

![device](doc/device.svg) In this picture, each square is a write block. A column of 6 write blocks makes up an erase block. 4 erase blocks make up the entire device.

### Log

Seismos creates a basic arrangement where the first write block (**Page**) of each filled erase block (**Block**) is special and contains the log of file system changes.

The log structure means that we don't have to use up flash endurance rewriting data in place.

![log](doc/log.svg)

A filled block contains the log, as well as some data, and a checksum in each page. The checksum is critical, and a valid checksum is the only way we know a block or page contains data.

![block](doc/eblock.svg)

The log may contain information about deleted items. At the end of the log, a sheet name is added, together with data belonging to the sheet. The data is considered valid as long as the page checksum is valid, or until the block ends.

### Allocation

As mentioned in the "Log" section, Seismos doesn't rewrite data in place. That means every piece of log, even old deletions, must remain until a new block is written, cancelling them. An advantage is that it's harder to end up with a file system that's completely broken.

As a consequence, the file system must always know where to write next.

Simplicity first: blocks get allocated sequentially: first, the initial block at an arbitrary index, then the next block at an index one greater, until the end of device. Then, start at 0, until the initial block is reached.

### Checksum

Checksums help Seismos avoid the problem of remembering which blocks are allocated.

The first page in each block contains a checksum. A valid checksum means the block contains a log entry, and possibly some file data.  An invalid checksum means that the log ended.

This makes error correction extremely important: an unrecoverable block will cause all the following data to be silently thrown away.

The checksum is actually composed of two parts: a (CRC32) checksum, and a sequence key, which is XORed with the basic checksum.

The checksum determines the root block: the root block is the only block where the checksum is valid when using the initial key. The key for each following block is the checksum of the previous block (TODO: currently, it's just the next sequential number).

Page checksums work in a similar way: an invalid page means that there is no more file data. There's no need to mark a "root page", so the key is shared between all pages in a block.

### Branches

Branches are how Seismos stores data that hasn't changed in a while. The root block may contain pointers to older blocks that begin a chain of blocks containing some important data.

TODO: only useful with >1 file.

## Status

Rather messy. Only emulated backend provided. Unfinished and limited. Reliability features not tested.

I'll add features as I need them.

## License

Licensed under either of

    Apache License, Version 2.0 (LICENSE-APACHE or http://www.apache.org/licenses/LICENSE-2.0)
    MIT license (LICENSE-MIT or http://opensource.org/licenses/MIT)

at your option.
