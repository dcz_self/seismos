use core::fmt::Debug;
use crc::{Crc, CRC_32_ISCSI};
use crate::util::u32_to_arr;

pub const CASTAGNOLI: Crc<u32> = Crc::<u32>::new(&CRC_32_ISCSI);

pub trait Key : Copy + Debug + PartialEq {
    fn new_null() -> Self;
    fn next(&self) -> Self;
}

#[derive(Clone, Copy, Debug, PartialEq)]
pub struct PrngKey(u32);

impl PrngKey {
    /// Totally random. https://xkcd.com/221/
    fn as_nonce(&self) -> [u8; 4] {
        if self.0 == 0 { [0; 4] }
        // TODO: derive key
        else { u32_to_arr(self.0) }
    }
}

impl Key for PrngKey {
    fn new_null() -> Self {
        Self(0)
    }
    fn next(&self) -> Self {
        Self(self.0 + 1)
    }
}

pub trait Checksum : Eq + core::fmt::Debug {
    const BYTES_COUNT: usize;
    type Key: Key;
    fn checksum(data: &[u8]) -> Self;
    fn from_raw(bytes: &[u8]) -> Self;
    fn as_bytes(&self) -> &[u8];
    fn xor(&self, key: Self::Key) -> Self;
}

#[derive(PartialEq, Eq, Debug)]
pub struct Crc32([u8; 4]);

impl Checksum for Crc32 {
    const BYTES_COUNT: usize = 4;
    
    type Key = PrngKey;
    
    fn checksum(data: &[u8]) -> Self {
        let cs = CASTAGNOLI.checksum(data);
        Self(u32_to_arr(cs))
    }
    fn from_raw(bytes: &[u8]) -> Self {
        let mut d = [0; 4];
        d[0] = bytes[0];
        d[1] = bytes[1];
        d[2] = bytes[2];
        d[3] = bytes[3];
        Self(d)
    }
    fn as_bytes(&self) -> &[u8] {
        &self.0
    }
    fn xor(&self, key: Self::Key) -> Self {
        let x = key.as_nonce();
        Self([
            self.0[0] ^ x[0],
            self.0[1] ^ x[1],
            self.0[2] ^ x[2],
            self.0[3] ^ x[3],            
        ])
    }
}