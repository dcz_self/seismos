/*! Raw storage device */

use core::fmt;
use core::fmt::Debug;
use core::marker::PhantomData;


#[derive(Clone, Copy, Debug)]
pub struct BlockIdx<Geometry>(pub usize, pub Geometry);

impl<Geom: Geometry> BlockIdx<Geom> {
    pub fn iter_cycle(self) -> BlockIter<Geom> {
        BlockIter(self)
    }
    pub fn iter_all(self) -> BlockIterOnce<Geom> {
        BlockIterOnce{current: self, end: None}
    }
    pub fn next(self) -> Self {
        Self((self.0 + 1) % self.1.get_block_count(), self.1)
    }
}

impl<Geom: Geometry> PartialEq for BlockIdx<Geom> {
    fn eq(&self, other: &Self) -> bool {
        self.0.eq(&other.0)
    }
}

pub struct BlockIter<Geometry>(BlockIdx<Geometry>);

impl<Geom: Geometry> Iterator for BlockIter<Geom> {
    type Item = BlockIdx<Geom>;
    fn next(&mut self) -> Option<Self::Item> {
        let ret = self.0;
        self.0 = self.0.next();
        Some(ret)
    }
}

pub struct BlockIterOnce<Geometry> {
    current: BlockIdx<Geometry>,
    end: Option<BlockIdx<Geometry>>,
}

impl<Geom: Geometry> Iterator for BlockIterOnce<Geom> {
    type Item = BlockIdx<Geom>;
    fn next(&mut self) -> Option<Self::Item> {
        let ret = self.current;
        match self.end {
            None => { self.end = Some(ret); },
            Some(end) => {
                if ret == end {
                    return None;
                }
            },
        }
        self.current = self.current.next();
        Some(ret)
    }
}

#[derive(Clone, Copy, Debug)]
pub struct PageIdx<Geometry>(
    pub BlockIdx<Geometry>,
    pub usize, // page
);
impl<Geom: Geometry> PageIdx<Geom> {
    pub fn next(self) -> Self {
        let page = (self.1 + 1) % self.0.1.get_page_count();
        let block
            = if page == 0 { self.0.next() }
            else { self.0 };
        Self(block, page)
    }
}
pub trait Geometry: Copy + Debug + PartialEq {
    const BLOCKS_IN_DEVICE_COUNT: usize;
    const PAGES_IN_BLOCK_COUNT: usize;
    const BYTES_IN_PAGE_COUNT: usize;
    fn get_block_count(&self) -> usize {
        Self::BLOCKS_IN_DEVICE_COUNT
    }
    fn get_page_count(&self) -> usize {
        Self::PAGES_IN_BLOCK_COUNT
    }
}

impl Geometry for () {
    const BLOCKS_IN_DEVICE_COUNT: usize = 1;
    const PAGES_IN_BLOCK_COUNT: usize = 1;
    const BYTES_IN_PAGE_COUNT: usize = 1;
}

#[derive(Debug)]
pub struct P<const S: usize>{
    data: [u8; S],
}

pub trait Page : Debug {
    fn uninitialized() -> Self;
    fn get_data(&self) -> &[u8];
    fn get_data_mut(&mut self) -> &mut [u8];
}

impl<const S: usize> Page for P<S> {
    fn uninitialized() -> Self {
        Self { data: [0; S] }
    }
    fn get_data(&self) -> &[u8] {
        &self.data
    }
    fn get_data_mut(&mut self) -> &mut [u8] {
        &mut self.data
    }
}

/// Those const would be better as associated, but Rust doesn't like it.
pub trait Device {
    type Page: Page;
    type Geometry: Default + core::fmt::Debug + Geometry;
    fn read_page(&self, index: PageIdx<Self::Geometry>, data: &mut Self::Page);
    fn write_page(&mut self, index: PageIdx<Self::Geometry>, data: &Self::Page);
    fn erase_block(&mut self, index: BlockIdx<Self::Geometry>);
}

#[cfg(feature="std")]
pub mod ram {
    use super::*;
    pub struct RAM<const BPD: usize, const PPB: usize, const BPP: usize> {
        data: Vec<u8>,
        shape: PhantomData<([(); BPP], [(); PPB], [(); BPD])>,
    }

    impl<const BPP: usize, const PPB: usize, const BPD: usize> RAM<BPP, PPB, BPD> {
        pub fn new() -> Self {
            Self {
                data: vec![0; BPP*PPB*BPD],
                shape: Default::default(),
            }
        }
    }
    
    impl<const BPP: usize, const PPB: usize, const BPD: usize> Device for RAM<BPD, PPB, BPP> {
        type Page = P<BPP>;
        type Geometry = FlatGeom<BPD, PPB, BPP>;

        fn read_page(&self, index: PageIdx<Self::Geometry>, data: &mut Self::Page) {
            let byte_offset = (index.0.0 * PPB + index.1) * BPP;
            data.data
                .copy_from_slice(&self.data[byte_offset..(byte_offset + BPP)]);
        }
        fn write_page(&mut self, index: PageIdx<Self::Geometry>, data: &Self::Page) {
            let byte_offset = (index.0.0 * PPB + index.1) * BPP;
            self.data[byte_offset..(byte_offset + BPP)]
                .copy_from_slice(&data.data);
        }
        fn erase_block(&mut self, index: BlockIdx<Self::Geometry>) {
            let byte_offset = (index.0 * PPB) * BPP;
            self.data[byte_offset..(byte_offset + BPP)]
                .iter_mut()
                .for_each(|i| *i = 0);
        }
    }
}

#[derive(Default, Clone, Copy, PartialEq)]
pub struct FlatGeom<const BPD: usize, const PPB: usize, const BPP: usize>(
    PhantomData<([(); BPD], [(); PPB], [(); BPP])>
);

impl<const BPD: usize, const PPB: usize, const BPP: usize> Geometry for FlatGeom<BPD, PPB, BPP> {
    const BLOCKS_IN_DEVICE_COUNT: usize = BPD;
    const PAGES_IN_BLOCK_COUNT: usize = PPB;
    const BYTES_IN_PAGE_COUNT: usize = BPP;
}

impl<const BPD: usize, const PPB: usize, const BPP: usize> Debug for FlatGeom<BPD, PPB, BPP> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> Result<(), fmt::Error> {
        write!(f, "1 Device {} blocks {} pages {} bytes", BPD, PPB, BPP)
    }
}