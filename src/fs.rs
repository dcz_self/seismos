
struct Seismos;

#[derive(Clone, Copy, Debug)]
struct FileId(u32);
#[derive(Clone, Copy, Debug)]
struct SheetId(FileId, usize);

/// Should not exceed one page
struct Header {
    bad_blocks: Vec<BadBlock>,
    branches: Vec<Branch>,
    erased: Vec<FileId>,
    current_stream: StreamId,
}

fn find_root(device: &BlockDevice) -> Option<(BlockIdx, Page)> {
    for block_idx in device.iter_blocks() {
        let page0 = device.read(Page(block_idx, 0));
        let (data, hash) = &page0.split_at(BLOCK_SIZE - HASH_LEN);
        if hash(data) == hash {
            return Some((block_idx, page0));
        }
    }
    None
}

/// Returns the first item *not* in the chain
fn find_chain_end<Shape>(
    device: &BlockDevice<Shape>,
    start: BlockIdx<Shape>,
    start_key: Key,
    _occupancy: OccupancyMap<Shape>,
) -> BlockIdx<Shape> {
    for block_idx in Shape::cycle_from(start + 1) {
        let page0 = device.read(Page(block_idx, 0));
        let (data, hash) = &page0.split_at(BLOCK_SIZE - HASH_LEN);
        if hash(data) != hash ^ get_key(start_key, block_idx - start) {
            return block_idx;
        }
    }
    unreachable!();
}

fn append_file(device: &BlockDevice, root_idx: BlockIdx, end_idx: BlockIdx, file: FileId) -> FileCursor {
    let find_block = |file| {
        for block_idx in (root_idx..end_idx).rev() {
            let page0 = device.read(PageIdx(block_idx, 0));
            let (data, hash) = &page0.split_at(BLOCK_SIZE - HASH_LEN);
            if Header::from_page(&page0).current_file == file {
                return Some((block_idx, hash));
            }
        }
        None
    };

    /// Actually, better not use this.
    // Files should be better composed of multiple streams,
    // each stream starting in a new block,
    // each stream started by a new append session.
    let find_empty_page = |block_idx, block_hash| {
        for page_idx in block_idx.pages_from(1) {
            let page = device.read(page_idx);
            let (data, hash) = &page0.split_at(BLOCK_SIZE - HASH_LEN);
            if get_hash(data) != hash ^ block_hash {
                // Must be writeable, cause the entire block was wiped,
                // and writing started from index 0.
                return Some(page_idx);
            }
        }
        None
    };
    
    match find_block(file) {
        Some((block_idx, hash)) => match find_empty_page(block_idx, hash) {
            Some(page_idx) => FileCursor::Page(page_idx, hash),
            None => FileCursor::Block(block_idx, key(block_idx - root_idx)),
        },
        None => Append::Block(end_idx, key(end_idx - root_idx)),
    }
}

fn open_stream(device: &BlockDevice, root_idx: BlockIdx, end_idx: BlockIdx, file: FileId) -> StreamCursor {
    for block_idx in (root_idx..end_idx).rev() {
        let page0 = device.read(PageIdx(block_idx, 0));
        let (data, hash) = &page0.split_at(BLOCK_SIZE - HASH_LEN);
        let header = Header::from_page(data);
        if header.current_stream.file == file {
            return StreamCursor::StartBlock(
                block: block_idx,
                block_key: key(block_idx - root_idx),
                stream_id: header.current_stream.next_stream(),
            );
        }
    }
    StreamCursor::StartBlock(
        block: end_idx,
        block_key: key(end_idx - root_idx),
        stream_id: StreamId::new(file),
    )
}

/// MAYBE: This needs semantics fixed: the block should get reserved
/// as soon as the stream is created.
/// Then all accepted data are guaranteed to be committed
/// even in case of error.
fn append_to_stream(
    device: &BlockDevice,
    mut stream: Stream,
    data: &[u8],
    occupied_blocks: OccupancyMap,
) -> Result<Stream, (Stream, usize, Error)> {
    match stream.cursor {
        StartBlock { block, block_key, stream_id } => {
            let block = reserve_block(device, root_idx, block, occupied_blocks);
            match block {
                Ok(block) => {
                    stream.page_buffer.add_header(stream_id);
                    let page = PageIdx(block, 0);
                    append_to_stream(
                        device,
                        Stream {
                            cursor: StreamCursor::ContinuePage {
                                page,
                                page_key: block_key,
                                stream_id,
                            },
                            ..stream
                        },
                        data,
                        occupied_blocks,
                    )
                },
                Err(e) => Err(stream, data.length(), e),
            }
        },
        ContinuePage { page, page_key, stream_id } => {
            let not_added = stream.page_buffer.add_data(data);
            if let BufferState::Full(not_added) = not_added {
                if let Occupancy::Occupied = occupied_blocks.get_status(stream.cursor.block) {
                    Err((stream, not_added.length(), Error::NoSpace))
                } else {
                    device.write_page(page, stream.page_buffer.buffer);
                    stream.page_buffer.clear();
                    let page = page.next();
                    let cursor
                        = if page.1 == 0 {
                            StreamCursor::StartBlock {
                                block: page.0,
                                block_key,
                                stream_id,
                            }
                        } else {
                            StreamCursor::ContinuePage {
                                page,
                                page_key,
                                stream_id,
                            },
                        };

                    append_to_stream(
                        device,
                        Stream {
                            cursor,
                            ..stream
                        },
                        not_added,
                        occupied_blocks,
                    )
                }
            } else {
                Ok(stream)
            }
        },
    }
}

fn reserve_block(
    device: &BlockDevice,
    root_idx: BlockIdx,
    end_idx: BlockIdx,
    occupied_blocks: OccupancyMap,
) -> Result<BlockIdx, Error> {
    if root_idx == end_idx {
        // This is a very bad situation:
        // there is no place left to write a new root block.
        // It can be recovered from in only 1 way: delete the end of a chain.
        // Attempts to free blocks in the middle of a chain
        // will cause a temporary loss of connectivity to the end,
        // which breaks the guarantee that power failures don't erase old data.
        //
        // For now, without multiple files, any delete will clear the entire device.
        // TODO: try garbage collection to free a block
        // After garbage is collected,
        // we write a new root block in the freed space.
        // For a moment, there are two root blocks,
        // so they must be ranked by a sequence number.
        // The new block inherits the old root chain as a new side chain,
        // plus all properties of the original block.
        Err(Error::NoSpace)
    } else if occupied_blocks.contains(end_idx) {
        // A side chain is in the way.
        // TODO: Either try to get rid of it, or jump over it.
        // For now, without multiple files, this is never going to happen.
        Err(Error::NotImplemented)
    } else {
        Ok(end_idx)
    }
}