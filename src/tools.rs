/*!
 * A collection of storage system filters for NAND flash memory.
 *
 * The system is meant for logging long streams of data to NAND without a Flash Translation Layer. Like a GPS logger.
 *
 * ## Constraints
 * - no FTL, so log-structured data
 * - low memory usage: streaming bytes directly into storage must remain possible
 * - writing is done when operating on battery power, so must be efficient
 * - must remain consistent after unexpected power loss
 * - power loss should cause minimal data loss
 */

/// Counterpart to Iterator.
pub trait Consumator {
    type NextValue;
    type NextResult;
    type CollectResult;
    /// Feeds another item.
    fn next(&mut self, value: Self::NextValue) -> Self::NextResult;
    /// Ends processing, returning the result.
    fn collect(self) -> Self::CollectResult;
}

struct BlockNand {
    block_size: usize,
    block_count: usize,
}

// must_use makes calling Consumator::collect() required
#[must_use]
struct NandLog;

impl Consumator for NandLog {
    type NextValue = &'static [u8, 4096];
    type NextResult = Result<(), Busy>;
    type CollectResult = ();
    
    fn new() -> Self {
        self.find_occupancy()
    }
    
    fn next(&mut self, value: Self::NextValue) -> Self::NextResult {
        next_block = self.current.next();
        if !next_block.empty() {
            next_block.erase();
        }
        next_block.write(value);
    }
}

/// Two buffers for writing
struct DoubleBuffering;

struct SliceIntoSectors<const SIZE: usize>;

struct AddEcc<const SIZE: usize>;