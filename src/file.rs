#[cfg(feature="std")]
use std::io;
#[cfg(not(feature="std"))]
use crate::io;
use crate::{FileId, Seismos, SheetHandle, SheetId};
use crate::checksum::Checksum;
use crate::storage;

pub struct WriteSheet<'a, BlockDevice, C>
where
    BlockDevice: storage::Device,
    C: Checksum,
{
    handle: Option<SheetHandle<BlockDevice, C>>,
    fs: &'a mut Seismos<BlockDevice, C>,
}

impl<'a, BlockDevice, C> WriteSheet<'a, BlockDevice, C>
where
    BlockDevice: storage::Device,
    C: Checksum,
{
    pub fn start(fs: &'a mut Seismos<BlockDevice, C>, id: FileId) -> Self {
        Self {
            handle: Some(Seismos::<BlockDevice, C>::start_sheet(id)),
            fs
        }
    }
}

impl<'a, BlockDevice, C> io::Write for WriteSheet<'a, BlockDevice, C>
where
    BlockDevice: storage::Device,
    C: Checksum,
{
    fn write(&mut self, buf: &[u8]) -> Result<usize, io::Error> {
        let mut handle = self.handle.as_mut().unwrap();
        self.fs.write_to_sheet(&mut handle, buf)
            .map_err(|e| io::ErrorKind::Other.into())
            .map(|()| buf.len())
    }
    fn flush(&mut self) -> Result<(), io::Error> {
        // flushing effectively ends the file, so no
        Ok(())
    }
}

impl<'a, BlockDevice, C> Drop for WriteSheet<'a, BlockDevice, C>
where
    BlockDevice: storage::Device,
    C: Checksum,
{
    fn drop(&mut self) {
        self.fs.finish_sheet(self.handle.take().unwrap());
    }
}

pub fn does_sheet_exist<D, C>(
    fs: &Seismos<D, C>,
    sheet: SheetId,
) -> bool
where
    D: storage::Device,
    C: Checksum,
{
    let mut found = false;
    fs.read_from_sheet(
        sheet,
        &mut |buf| {
            found = true;
            false
        },
    );
    found
}

pub fn does_file_exist<D, C>(
    fs: &Seismos<D, C>,
    file: FileId,
) -> bool
where
    D: storage::Device,
    C: Checksum,
{
    does_sheet_exist(fs, SheetId(file, 0))
}

pub fn count_sheets<D, C>(
    fs: &Seismos<D, C>,
    file: FileId,
) -> u32
where
    D: storage::Device,
    C: Checksum,
{
    let mut i = 0;
    while does_sheet_exist(fs, SheetId(file, 0)) {
        i += 1;
    }
    i
}