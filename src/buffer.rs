use core::cmp;
use core::marker::PhantomData;
use crate::Error;
use crate::checksum::Checksum;
use crate::storage::Page;

/// Circular buffer, although we only need an inchworm buffer:
/// https://commons.wikimedia.org/wiki/File:A_moving_caterpillar.webm
pub struct PageWriteBuffer<P: Page, C: Checksum> {
    /// Not actually used for its pageness,
    /// but for the correct sizing of the buffer inside.
    /// It's impossible to create a correctly statically sized buffer
    /// out of a Geometry trait today.
    page: P,
    /// (start, end), end is always greater than start by the buffer length.
    cursor: Option<(usize, usize)>,
    /// Used to subtract the checksum length
    /// from available buffer space.
    checksum: PhantomData<C>,
}

impl<P: Page, C: Checksum> PageWriteBuffer<P, C> {
    pub fn new() -> Self {
        Self {
            page: P::uninitialized(),
            cursor: None,
            checksum: Default::default(),
        }
    }
    
    /// Total available bytes
    pub fn get_space(&self) -> usize {
        self.page.get_data().len() - C::BYTES_COUNT
    }
    
    /// Occupied bytes
    pub fn get_length(&self) -> usize {
        self.cursor
            .map(|(start, end)| end - start).unwrap_or(0)
    }
    
    /// Returns how many bytes were *not* added.
    pub fn add_data(&mut self, data: &[u8]) -> usize {
        // This case would result in a special case
        // to create the None cursor anyway
        if data.len() == 0 {
            return 0;
        }
        let (start, end) = match self.cursor {
            None => (0, 0),
            Some(d) => d,
        };

        let space = self.get_space();
        let dest = self.page.get_data_mut();
        (end..(start + space))
            .zip(data.iter())
            .for_each(|(i, d)| dest[i % space] = *d);
        let written = cmp::min(data.len(), start + space - end);
        self.cursor = Some((start, end + written));
        data.len() - written
    }

    pub fn try_add_data(&mut self, data: &[u8]) -> Result<(), Error> {
        if self.get_space() - self.get_length() >= data.len() {
            self.add_data(data);
            Ok(())
        } else {
            Err(Error::NoSpace)
        }
    }

    /// Consumes bytes by reading them out into the buffer.
    /// Returns consumed bytes.
    pub fn read_out(&mut self, buffer: &mut [u8]) -> usize {
        match self.cursor {
            None => 0,
            Some((start, end)) => {
                let src = self.page.get_data();
                let space = self.get_space();
                (start..end)
                    .zip(buffer.iter_mut())
                    .for_each(|(i, d)| *d = src[i % space]);
                let written = cmp::min(end - start, buffer.len());
                self.cursor
                    = if written == end - start { None }
                    else {
                        let new_start = start % space;
                        Some((new_start, new_start + end - start))
                    };
                written
            }
        }
    }
}
