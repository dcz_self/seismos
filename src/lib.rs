/*! Seismos: an ad-hoc file system for append-mostly data
on solid-state storage.

"Seismos" = "quake". It's for 3 reasons:
- because it appends data, like a seismograph on a sheet of rolled paper,
- because it was made to record accelerometer readings,
- because at some point, it will need to handle flaky connections, like an SD card pins disconnecting under shocks.

Appearing:
- Flash Storage
- Erase Block
- Write Page

In supporting wooden roles:
- Branch (Chain): a logically contiguous collection of blocks
- Trunk: the only branch that is allowed to grow
- Root: the first block of the trunk branch
- File
- Sheet: a single recording within the file
- Log: the data stored in consecutive blocks

Order of log traversal:
1. Start randomly, identify Root.
2. Follow branches, if necessary see 2.
3. Follow current branch.

Problems:
- root header must always be written atomically. If not, side chains could end up getting overwritten
- block headers must always be written atomically. If not, sheets falling into the second page could end up getting mixed.
- deletion of a file must prevent any write operations on it from continuing.
- EASY: key for 2nd block in trunk must be derived from root hash. If 2nd block key is always the same, stale blocks next to a rewritten root will stay valid.
 */

#![cfg_attr(not(feature="std"), no_std)]

mod buffer;
pub mod checksum;
pub mod file;
#[cfg(not(feature="std"))]
pub mod io;
pub mod storage;
mod util;
use arrayvec::ArrayVec;
use core::fmt;

use buffer::PageWriteBuffer;
use checksum::{Checksum, Key};
use storage::{BlockIdx, PageIdx};
use storage::{Page, Geometry};
use util::{serialize_into, u16_to_arr, u32_to_arr, u32_from_buf};

#[derive(Clone, Copy, PartialEq, Debug)]
pub struct FileId(pub u32);

impl FileId {
    fn serialize(&self, buf: &mut [u8]) -> usize {
        buf[0..4].copy_from_slice(&u32_to_arr(self.0));
        4
    }
    fn from_buffer(buf: &[u8]) -> Self {
        FileId(u32_from_buf(buf))
    }
}

#[derive(Clone, Copy, PartialEq, Debug)]
pub struct SheetId(pub FileId, pub u32);

impl SheetId {
    /// Doesn't represent any file.
    /// Marks that the header won't be followed by any data.
    /// Not allowed in the userspace.
    const NO_SHEET: SheetId = SheetId(FileId(0), 0);
    /// Selects all sheets belonging to the file.
    /// This sheet number is reserved.
    //const ANY: u32 = u32::MAX;
    
    fn serialize(&self, buf: &mut [u8]) -> usize {
        buf[0..4].copy_from_slice(&u32_to_arr(self.0.0));
        buf[4..8].copy_from_slice(&u32_to_arr(self.1));
        8
    }
    
    fn from_buffer(buf: &[u8]) -> Self {
        SheetId(FileId(u32_from_buf(buf)), u32_from_buf(&buf[4..]))
    }
}

pub struct SheetHandle<D: storage::Device, C: Checksum> {
    buffer: PageWriteBuffer<D::Page, C>,
    state: SheetState<D::Geometry, C::Key>,
}

impl<D: storage::Device, C: Checksum> SheetHandle<D, C> {
    fn new(file: FileId) -> Self {
        Self {
            buffer: PageWriteBuffer::new(),
            state: SheetState::Unanchored(file),
        }
    }
}

#[derive(Clone)]
enum SheetState<G: Geometry, Key> {
    /// No trace on the FS yet
    Unanchored(FileId),
    /// Describes last piece written
    Placed(PageIdx<G>, Key, SheetId),
}

#[derive(Clone, Debug, PartialEq)]
struct BlockLog<G: Geometry, Key> {
    // only root will have bad blocks or chains though
    chains: [(BlockIdx<G>, Key); 0],
    bad_blocks: [BlockIdx<G>; 0],
    /// Deletions must be inline with actual file data,
    /// because file data presence constitutes file creation.
    /// Deletions and creations must be possible to order
    /// relative to each other.
    ///
    /// Deletions apply *after* chains.
    /// There must not be stale data in the deletion log,
    /// because that could mistakenly erase creations in branches.
    deletions: ArrayVec<FileId, 16>,
}

impl<G: Geometry, K: fmt::Debug> BlockLog<G, K> {
    fn new() -> Self {
        Self {
            chains: [],
            bad_blocks: [],
            deletions: Default::default(),
        }
    }
    fn with_file_deletion(mut self, file: FileId) -> (Self, Option<FileId>) {
        let push = self.deletions.try_push(file);
        (
            self,
            match push {
                Err(_) => Some(file),
                Ok(()) => None,
            }
        )
    }

    fn from_buffer(buf: &[u8]) -> (Self, usize) {
        let count = u16::from_be_bytes([buf[0], buf[1]]);
        let mut offset = 2;
        let mut s = Self::new();
        for _i in 0..count {
            s = s.with_file_deletion(FileId::from_buffer(&buf[offset..])).0;
            offset += 4;
        }
        (s, offset)
    }
    fn is_root(&self) -> bool {
        self.chains.len() > 0 || self.bad_blocks.len() > 0
    }
    /// Bytes in data contained inside
    fn get_payload_length(&self) -> u16 {
        self.deletions.len() as u16 * 4
    }
    /// serialized length
    fn get_length(&self) -> u16 {
        2 + self.get_payload_length()
    }
    /// Returns number of bytes that were serialized
    fn serialize(&self, buffer: &mut [u8]) -> usize {
        buffer.iter_mut()
            .zip(u16_to_arr(self.deletions.len() as u16).iter())
            .for_each(|(d, s)| *d = *s);
        let mut offset = 2;
        for deletion in &self.deletions {
            offset += deletion.serialize(&mut buffer[offset..]);
        }
        offset
    }
}

#[derive(Debug, PartialEq)]
struct BlockHeader<G: Geometry, K> {
    log: BlockLog<G, K>,
    sheet: SheetId, // 0:0 means no data follows. For page=block
    // if header splits over into another page, it's written immediately,
    // overflow pages without file contents
}

impl<G: Geometry, Key: fmt::Debug> BlockHeader<G, Key> {
    fn with_file_deletion(self, file: FileId) -> (Self, Option<FileId>) {
        let (log, res) = self.log.with_file_deletion(file);
        (
            Self {
                log,
                ..self
            },
            res,
        )
    }

    // Will panic if the buffer is too short.
    // And the buffer *will* be too short if page size is short.
    // TODO: spill over multiple pages?
    fn serialize(&self, buffer: &mut [u8]) -> usize {
        buffer[0..4].copy_from_slice(b"SESM");
        buffer[4] = 1; // version
        let offset = 5;
        let offset = offset + self.log.serialize(&mut buffer[offset..]);
        offset + self.sheet.serialize(&mut buffer[offset..])
    }
    fn get_length(&self) -> usize {
        4 + 1 + self.log.get_length() as usize + 8
    }
    fn serialize_into(&self, _buffer: &mut [u8]) -> usize {
        // This will be useful for multi-page headers
        todo!();
    }
    
    fn from_buffer(buffer: &[u8]) -> Result<Self, &'static str> {
        if &buffer[..4] != b"SESM" {
            return Err("Invalid buffer")
        }
        if buffer[4] != 1 {
            return Err("Unsupported log version")
        }
        let offset = 5;
        let (log, read) = BlockLog::from_buffer(&buffer[offset..]);
        let offset = offset + read;
        Ok(Self {
            log,
            sheet: SheetId::from_buffer(&buffer[offset..]),
        })
    }
}

type LogEntry<G, K> = BlockHeader<G, K>;

fn page_extract_payload<C: Checksum, P: Page>(
    page: &P,
    offset: usize,
) -> &[u8] {
    let data = page.get_data();
    &data[offset..(data.len() - C::BYTES_COUNT)]
}

#[derive(Debug, PartialEq)]
pub enum Error {
    NoSpace,
}

#[derive(Clone, Debug)]
enum NextBlock<G: Geometry, K> {
    Open(BlockIdx<G>, K),
    /// Nowhere to put the next block! We're out of space.
    /// But at least we found the block that blocks.
    /// This will be a root of some chain.
    Occupied(BlockIdx<G>, K),
}

#[derive(Clone, Debug)]
struct State<G: Geometry, Key> {
    root_position: Option<BlockIdx<G>>,
    // Cached values
    // Should it already be erased?
    next_block: NextBlock<G, Key>,
    // Stores the next entry in the metadata log,
    // not the current metadata summary.
    // Summary is gathered by traversing all chains.
    next_meta_header: BlockLog<G, Key>,
}

impl<G: Geometry, K: checksum::Key + fmt::Debug> State<G, K> {
    fn next_block_written<BlockDevice, C: Checksum<Key=K>>(
        mut self,
        device: &BlockDevice,
        mut buffer: &mut BlockDevice::Page,
    ) -> Self
        where BlockDevice: storage::Device<Geometry=G>
    {
        use NextBlock::*;
        
        match self.next_block {
            Occupied(..) => panic!("Occupied block reported written"),
            Open(written_pos, key) => {
                let next_root
                    = if self.next_meta_header.is_root() {
                        Some(written_pos)
                    } else {
                        self.root_position.or(Some(written_pos))
                    };

                let (next_pos, key) = Seismos::<BlockDevice, C>::find_invalid_block(device, &mut buffer, written_pos, key);

                let key
                    = if next_root == Some(next_pos) { C::Key::new_null() }
                    else { key };

                // TODO: check against occupancy map
                if Some(next_pos) == self.root_position {
                    // Oh no, we're out of space.
                    // TODO: Need to try clearing stale stuff.
                    self.next_block = NextBlock::Occupied(next_pos, key);
                    // This root is born without chains.
                    // That's because it's unknown where the chains will point,
                    // until a deletion is made.
                    // TODO: copy bad blocks from old root.
                    self.next_meta_header = BlockLog::new();
                } else {
                    self.next_block = NextBlock::Open(next_pos, key);
                    self.next_meta_header = BlockLog::new();
                }
                self.root_position = next_root;
                self
            },
        }
    }
    
    fn log_file_deletion(self, file: FileId) -> (Self, Option<FileId>) {
        let (next_meta_header, ret)
            = self.next_meta_header.with_file_deletion(file);
        (
            Self {
                next_meta_header,
                ..self
            },
            ret,
        )
    }
}

#[derive(Debug)]
enum LogEntryValidity {
    Valid,
    Invalid,
}

enum PageValidity {
    Valid,
    Invalid,
}

#[derive(Debug)]
enum FileState<G> {
    /// File not present in the logs
    NotPresent,
    /// Not present in the file system, but present in the log
    Deleted,
    /// Pressent in the file system.
    Present(BlockIdx<G>),
}

pub struct Seismos<D: storage::Device, C: Checksum + core::fmt::Debug>{
    device: D,
    state: State<D::Geometry, C::Key>,
}

impl<BlockDevice, C> Seismos<BlockDevice, C>
    where
    BlockDevice: storage::Device,
    C: Checksum + core::fmt::Debug,
{
    pub fn new(device: BlockDevice) -> Self {
        let mut page_buffer = BlockDevice::Page::uninitialized();
        let root_position = Self::find_root(&device, &mut page_buffer);
        let (next_block, next_meta_header)
            = match root_position {
                Some(root_position) => {
                    let (next_pos, key) = Self::find_invalid_block(
                        &device,
                        &mut page_buffer,
                        root_position.next(),
                        C::Key::new_null(),
                    );
                    if next_pos == root_position {
                        // Oh no, we're out of space.
                        // Need to try deleting stuff.
                        unimplemented!();
                    } else {(
                        NextBlock::Open(next_pos, key),
                        // Initially, an empty log entry.
                        // Extra data can be added as needed.
                        BlockLog::new(),
                    )}
                },
                None => (
                    NextBlock::Open(
                        BlockIdx(0, BlockDevice::Geometry::default()),
                        C::Key::new_null(),
                    ),
                    BlockLog::new(),
                ),
            };
        Self {
            device,
            state: State {
                root_position,
                next_block,
                next_meta_header,
            }
        }
    }

    /// Finds an invalid block, checking block at `pos` first against `key`.
    /// This WILL stop somewhere because of the incrementing key.
    /// Can't go around the device twice without the keys being different.
    fn find_invalid_block(
        device: &BlockDevice,
        mut buffer: &mut BlockDevice::Page,
        pos: BlockIdx<BlockDevice::Geometry>,
        mut key: C::Key,
    ) -> (BlockIdx<BlockDevice::Geometry>, C::Key) {
        for pos in pos.iter_cycle() {
            let validity = Self::log_read(device, &mut buffer, pos, key);
            if let LogEntryValidity::Invalid = validity {
                return (pos, key);
            }
            key = key.next();
        }
        unreachable!();
    }
    
    /// Must correbt the page if needed.
    fn page_read(
        device: &BlockDevice,
        mut buffer: &mut BlockDevice::Page,
        pos: PageIdx<BlockDevice::Geometry>,
        key: C::Key,
    ) -> PageValidity {
        device.read_page(pos, &mut buffer);
        let (data, hash) = buffer.get_data()
            .split_at(BlockDevice::Geometry::BYTES_IN_PAGE_COUNT - C::BYTES_COUNT);
        if C::checksum(data) == C::from_raw(hash).xor(key) {
            PageValidity::Valid
        } else { PageValidity::Invalid }
    }
    
    // TODO: should this parse the entire log entry?
    // To consider: what to do if the second page is not committed,
    // but the first page is valid?
    // The log entry is then incomplete.
    // Should the file system just deal with it,
    // or force discard of the entry?
    // Esp. the case of root - discard will necessarily have to happen.
    fn log_read(
        device: &BlockDevice,
        mut buffer: &mut BlockDevice::Page,
        pos: BlockIdx<BlockDevice::Geometry>,
        key: C::Key,
    ) -> LogEntryValidity {
        match Self::page_read(device, &mut buffer, PageIdx(pos, 0), key) {
            PageValidity::Valid => LogEntryValidity::Valid,
            PageValidity::Invalid => LogEntryValidity::Invalid,
        }
    }
    
    /// Returns parsed log, and also the data for the next block.
    /// Finding the next block is a bit wasteful
    /// due to the need to skip based on the occupancy map,
    /// but at least it doesn't involve any storage access.
    // TODO: how does the occupancy map work?
    // Should probably mark only non-trunk branches,
    // cause if it contains root, it can't be used to skip out-of-order blocks.
    fn log_traverse(
        device: &BlockDevice,
        mut buffer: &mut BlockDevice::Page,
        pos: BlockIdx<BlockDevice::Geometry>,
        key: C::Key,
    ) -> Option<(
        LogEntry<BlockDevice::Geometry, C::Key>,
        BlockIdx<BlockDevice::Geometry>,
        C::Key,
    )> {
        match Self::log_read(device, &mut buffer, pos, key) {
            LogEntryValidity::Invalid => None,
            LogEntryValidity::Valid => Some((
                LogEntry::from_buffer(buffer.get_data()).unwrap(),
                pos.next(),
                key.next(),
            ))
        }
    }
    
    /// Fnds first valid block. Unique to root.
    fn find_root(device: &BlockDevice, mut buffer: &mut BlockDevice::Page)
        -> Option<BlockIdx<BlockDevice::Geometry>>
    {
        let block_zero = BlockIdx(0, BlockDevice::Geometry::default());

        for block_idx in block_zero.iter_all() {
            let validity = Self::log_read(device, &mut buffer, block_idx, C::Key::new_null());
            if let LogEntryValidity::Valid = validity {
                return Some(block_idx);
            }
        }
        None
    }
    
    pub fn start_sheet(file: FileId) -> SheetHandle<BlockDevice, C> {
        SheetHandle::new(file)
    }

    pub fn write_to_sheet(
        &mut self,
        sheet: &mut SheetHandle<BlockDevice, C>,
        buf: &[u8],
    ) -> Result<(), Error> {
        self._write_to_sheet(sheet, buf, false)
    }
    
    /// Writes payload to the file system,
    /// as long as there is a page ready to fill.
    /// With `flush`, doesn't stop until payload is exhausted
    /// (thus placing the the sheet in a state where it can only be closed).
    fn _write_to_sheet(
        &mut self,
        sheet: &mut SheetHandle<BlockDevice, C>,
        buf: &[u8],
        // Forces a buffer flush.
        // This makes sense only when the file is closed.
        flush: bool,
    ) -> Result<(), Error> {
        let needs_page = match sheet.state {
            SheetState::Unanchored(file_id) => None,
            SheetState::Placed(page_idx, key, _sheet_id) => {
                if page_idx.next().1 == 0 { None }
                else { Some((page_idx, key)) }
            },
        };

        match needs_page {
            Some((page_idx, key)) => {
                self.write_sheet_pages(sheet, buf, flush)
            },
            None => {
                self.write_block_pages(sheet, buf, flush)
            },
        }
    }

    /// Tries to start a new block with the given sheet.
    /// *Does not* check if the block is actually needed.
    fn write_block_pages(
        &mut self,
        sheet: &mut SheetHandle<BlockDevice, C>,
        buf: &[u8],
        // Forces a buffer flush.
        // This makes sense only when the file is closed.
        flush: bool,
    ) -> Result<(), Error> {
        match &self.state.next_block {
            NextBlock::Occupied(_block, _key) => {
                // TODO: try deleting?
                if flush {
                    Err(Error::NoSpace)
                } else {
                    sheet.buffer.try_add_data(buf)
                }
            },
            // Not trying to write to the internal buffer first
            // because the header may be needed,
            // and the header is variable length.
            NextBlock::Open(block_idx, key) => {
                let key = *key;
                let block_header = BlockHeader {
                    log: self.state.next_meta_header.clone(),
                    sheet: SheetId(FileId(0), 0),
                };

                let header_bytes_count
                    // checksum may lie in the middle of any of the 3 buffers,
                    // but will always be part of the page.
                    = C::BYTES_COUNT
                    + block_header.get_length() as usize;
                let pending_bytes_count
                    = header_bytes_count
                    + sheet.buffer.get_length()
                    + buf.len();

                if header_bytes_count
                    > BlockDevice::Geometry::BYTES_IN_PAGE_COUNT
                {
                    // The header needs to be written in multiple pages, atomically.
                    // The last page will contain some empty space before actual data starts flowing in.
                    unimplemented!()
                }
                else if pending_bytes_count
                    >= BlockDevice::Geometry::BYTES_IN_PAGE_COUNT
                    || flush
                {
                    // A new block will be written
                    let page_idx = PageIdx(*block_idx, 0);

                    let total = block_header.get_length() as usize
                        + sheet.buffer.get_length()
                        + buf.len();
                    
                    let mut page = BlockDevice::Page::uninitialized();
                    
                    let sheet_id = match sheet.state {
                        SheetState::Unanchored(file_id)
                            => dbg!(self.find_free_sheet(file_id, &mut page))?,
                        SheetState::Placed(_page, _key, sheet_id) => sheet_id,
                    };
                    
                    let (payload_written, buf_consumed)
                        = Self::commit_log_page(
                            &mut self.device,
                            page_idx,
                            key,
                            block_header.log,
                            &mut sheet.buffer,
                            sheet_id,
                            buf,
                        ).unwrap();
                    
                    // Chain only needs the first page to be valid.
                    // Block header fully written, regenerate.
                    self.state = self.state.clone()
                        .next_block_written::<_, C>(&mut self.device, &mut page);
                    sheet.state = SheetState::Placed(page_idx, key, sheet_id);

                    if total > payload_written {
                        self._write_to_sheet(
                            sheet,
                            &buf[buf_consumed..],
                            flush,
                        )
                    } else {
                        Ok(())
                    }
                } else {
                    let remaining = sheet.buffer.add_data(buf);
                    if remaining > 0 {
                        panic!("BUG: added data to buffer even though it spills over")
                    }
                    Ok(())
                }
            },
        }
    }
    
    fn write_sheet_pages(
        &mut self,
        sheet: &mut SheetHandle<BlockDevice, C>,
        buf: &[u8],
        // Flush the last page. Useful for closing sheets
        flush: bool,
    ) -> Result<(), Error> {
        let page_bytes_count
            = C::BYTES_COUNT
            + sheet.buffer.get_length()
            + buf.len();
        if (page_bytes_count >= BlockDevice::Geometry::BYTES_IN_PAGE_COUNT)
            || flush {
            match sheet.state {
                SheetState::Placed(page_idx, key, sheet_id) => {
                    let page_idx = page_idx.next();
                    if page_idx.1 == 0 {
                        panic!("Can't continue into the next block in this procedure");
                    }
                    
                    let payload_pending = sheet.buffer.get_length() + buf.len();

                    let (payload_written, buf_consumed)
                        = Self::commit_nth_page(
                            &mut self.device,
                            &mut sheet.buffer,
                            page_idx,
                            key,
                            buf,
                        ).unwrap();

                    sheet.state = SheetState::Placed(page_idx, key, sheet_id);
                    if payload_pending > payload_written {
                        self._write_to_sheet(
                            sheet,
                            &buf[buf_consumed..],
                            flush,
                        )
                    } else {
                        Ok(())
                    }
                },
                _ => panic!("Unplaced sheet can't be written"),
            }
        } else {
            // It would be out of place if we just proved
            // that there isn't enough data to fill a page,
            // and then we overflowed the page buffer.
            Ok(sheet.buffer.try_add_data(buf).unwrap())
        }
    }

    /// Places a log page in storage, big enough or not.
    /// *Does not* write following pages.
    /// Returns number of bytes written,
    /// and number of bytes consumed from buf.
    fn commit_log_page(
        device: &mut BlockDevice,
        page_idx: PageIdx<BlockDevice::Geometry>,
        key: C::Key,
        log: BlockLog<BlockDevice::Geometry, C::Key>,
        sheet_buffer: &mut PageWriteBuffer<BlockDevice::Page, C>,
        sheet_id: SheetId,
        buf: &[u8],
    ) -> Result<(usize, usize), Error> {
        if page_idx.1 != 0 {
            unimplemented!("Can't write log into non-first pages. May never be possible.");
        }

        let block_idx = page_idx.0;
        
        let mut page = BlockDevice::Page::uninitialized();

        let block_header = BlockHeader {
            sheet: sheet_id,
            log,
        };

        let (payload, chksum) = page.get_data_mut()
            .split_at_mut(
                BlockDevice::Geometry::BYTES_IN_PAGE_COUNT
                - C::BYTES_COUNT
            );

        let page_idx = PageIdx(block_idx, 0);
        //self.next_block_header = BUSY;
        // The header may span pages, so not atomically written.
        // Don't regenerate contents until it's fully committed.
        
        let total = block_header.get_length() as usize
            + sheet_buffer.get_length()
            + buf.len();
        let offset = 0;
        
        // Header will always fit completely.
        let offset = offset
            + block_header.serialize(&mut payload[offset..]);
        let offset = offset
            + sheet_buffer.read_out(&mut payload[offset..]);
        let buf_consumed = serialize_into(&mut payload[offset..], buf);
        let payload_written = offset + buf_consumed;

        let cs = C::checksum(payload).xor(key);
        chksum.copy_from_slice(
            cs.as_bytes(),
        );
        
        device.write_page(page_idx, &page);
        Ok((payload_written, buf_consumed))
    }
    
    /// Places a non-log page in storage, big enough or not.
    /// *Does not* write following pages.
    /// Returns number of bytes written,
    /// and number of bytes consumed from buf.
    fn commit_nth_page(
        device: &mut BlockDevice,
        sheet_buffer: &mut PageWriteBuffer<BlockDevice::Page, C>,
        page_idx: PageIdx<BlockDevice::Geometry>,
        key: C::Key,
        buf: &[u8],
    ) -> Result<(usize, usize), Error> {
        if page_idx.1 == 0 {
            panic!("Can't write block header here");
        }
        
        let mut page = BlockDevice::Page::uninitialized();
        let (payload, chksum) = page.get_data_mut()
            .split_at_mut(
                BlockDevice::Geometry::BYTES_IN_PAGE_COUNT
                - C::BYTES_COUNT
            );
        let offset = 0;
        let offset = offset
            + sheet_buffer.read_out(&mut payload[offset..]);
        let buf_consumed = serialize_into(&mut payload[offset..], buf);
        let payload_written = offset + buf_consumed;
        
        let cs = C::checksum(payload).xor(key);
        chksum.copy_from_slice(
            cs.as_bytes(),
        );
        device.write_page(page_idx, &page);
        Ok((payload_written, buf_consumed))
    }
    
    pub fn finish_sheet(
        &mut self,
        mut sheet: SheetHandle<BlockDevice, C>,
    ) -> Result<(), (Error, SheetHandle<BlockDevice, C>)> {
        self._write_to_sheet(&mut sheet, &[], true)
            .map_err(|e| (e, sheet))
    }

    pub fn delete_file(
        &mut self,
        file_id: FileId,
    ) -> Result<(), Error> {
        let (state, failure) = self.state.clone().log_file_deletion(file_id);
        if let Some(_file) = failure {
            unimplemented!("Can't deal with that many deleted files"); 
        }
        self.state = state;
        // TODO: this solves the problem
        // of read/write operations checking the log, but is inelegant
        self.collect_garbage();
        self.sync_log()
    }

    pub fn sync_log(&mut self) -> Result<(), Error> {
        if self.state.next_meta_header == BlockLog::new() {
            Ok(())
        } else {
            self.write_log()
        }
    }
    
    #[allow(dead_code)]
    fn force_commit_log(&mut self) -> Result<(), Error> {
        self.write_log()
    }
    
    /// Writes current log entry, even if there isn't anything new.
    fn write_log(&mut self) -> Result<(), Error> {
        match self.state.next_block {
            NextBlock::Occupied(_block, _key) => {
                Err(Error::NoSpace)
            },
            NextBlock::Open(block_idx, key) => {
                dbg!("write_log", &self.state);
                let block_header = BlockHeader {
                    log: self.state.next_meta_header.clone(),
                    sheet: SheetId::NO_SHEET,
                };
                let header_bytes_count
                    // checksum may lie in the middle of any of the 3 buffers,
                    // but will always be part of the page.
                    = C::BYTES_COUNT
                    + block_header.get_length() as usize;

                if header_bytes_count
                    > BlockDevice::Geometry::BYTES_IN_PAGE_COUNT
                {
                    // The header needs to be written in multiple pages, atomically.
                    // The last page will contain some empty space before actual data starts flowing in.
                    unimplemented!("Header too long")
                } else {
                    // A new block will be written
                    let mut page = BlockDevice::Page::uninitialized();            
                    let (payload, chksum) = page.get_data_mut()
                        .split_at_mut(
                            BlockDevice::Geometry::BYTES_IN_PAGE_COUNT
                            - C::BYTES_COUNT
                        );

                    let page_idx = PageIdx(block_idx, 0);
                    let offset = 0;
                    
                    // Header will always fit completely.
                    let _offset = offset
                        + block_header.serialize(&mut payload[offset..]);
                    
                    chksum.copy_from_slice(
                        C::checksum(payload).xor(key).as_bytes(),
                    );
                    
                    self.device.write_page(page_idx, &page);
                    // Chain only needs the first page to be valid.
                    // Block header fully written, regenerate
                    self.state = self.state.clone()
                        .next_block_written::<_, C>(&mut self.device, &mut page);

                    dbg!("write_log_done", &self.state);
                    Ok(())
                }
            },
        }
    }
    
    fn _write_page_with_hash(
        device: &mut BlockDevice,
        idx: PageIdx<BlockDevice::Geometry>,
        data: &[u8],
        key: C::Key,
    ) {
        let (data, _) = data.split_at(BlockDevice::Geometry::BYTES_IN_PAGE_COUNT - C::BYTES_COUNT);
        let cs = C::checksum(data).xor(key);

        let mut page_buffer = BlockDevice::Page::uninitialized();
        {
            let (new_data, c)
                = page_buffer.get_data_mut().split_at_mut(
                    BlockDevice::Geometry::BYTES_IN_PAGE_COUNT - C::BYTES_COUNT
                );
            
            new_data.copy_from_slice(data);
            c.copy_from_slice(cs.as_bytes());
        }
        device.write_page(idx, &page_buffer);
    }
    
    /// Tries to find space.
    pub fn collect_garbage(&mut self) {
        /*
        1. find earliest file
        2. check if deleted by the end
        3. repeat until all files done
        4. place new root if current placement is unoccupied
        
        better? alternative: save the positions of first N-1 occupied files,
        or placement of M deleted files after the first present one,
        M+N matching what a log entry can hold in chains and deletions.
        Then turn each occupied into a new chain,
        and store each delete (in an extra chain inserted between the occupied files and the unanalyzed part).
        */
        if let Some(root_idx) = self.state.root_position { 
            let mut page_buffer = BlockDevice::Page::uninitialized();
            let mut traversal = LogTraversal::<_, C>::new(&self.device, root_idx);
            // log traversal
            while let Some((log_entry, current_idx, current_key))
                = traversal.next(&mut page_buffer)
            {
                let file_id = log_entry.sheet.0;
                // going quadratic time on blocks.
                // could go linear space on files.
                
                let existence
                    = if file_id == SheetId::NO_SHEET.0 {
                        FileState::Deleted // just stores deletions
                    } else {
                        self.find_file_start(file_id)
                    };
                match existence {
                    FileState::Present(_) => {
                        return; // No present files allowed at the time of deletion. This will blow up with 2 files though.
                        unimplemented!("need to deal with skipping present files");
                    },
                    _ => {
                        let (idx, k) = match self.state.next_block.clone() {
                            NextBlock::Occupied(idx, k) => (idx, k),
                            NextBlock::Open(idx, k) => (idx, k),
                        };
                        self.state.next_block
                            = if idx == current_idx {
                               NextBlock::Open(idx, k)
                            } else {
                                self.state.next_block.clone()
                            };
                    },
                }
            }
            // TODO: check occupancy map for next block
            self.state.root_position = None;
            self.state.next_block = match self.state.next_block.clone() {
                NextBlock::Occupied(idx, k) => NextBlock::Occupied(idx, C::Key::new_null()),
                NextBlock::Open(idx, k) => NextBlock::Open(idx, C::Key::new_null()),
            };
            self.sync_log();
        }
    }
    
    /// Traverses the entire log in search for deletion and creation events.
    fn find_file_start(&self, file_id: FileId)
        -> FileState<BlockDevice::Geometry>
    {
        dbg!("file start", &self.state);
        match self.state.root_position {
            None => FileState::NotPresent,
            Some(root_idx) => {
                let mut page_buffer = BlockDevice::Page::uninitialized();
                let mut traversal = LogTraversal::<_, C>::new(&self.device, root_idx);
                let mut exists = FileState::NotPresent;
                // log traversal
                while let Some((log_entry, current_idx, next_entry_key))
                    = traversal.next(&mut page_buffer)
                {
                    dbg!(&log_entry);
                    dbg!(current_idx);
                    if let Some(_) = log_entry.log.deletions.iter().find(|d| **d == file_id) {
                        exists = FileState::Deleted;
                    }
                    if log_entry.sheet.0 == file_id {
                        exists = FileState::Present(
                            if let FileState::Present(idx) = exists { idx }
                            else { current_idx }
                        );
                    }
                }
                if let Some(_) = self.state.next_meta_header.deletions.iter().find(|d| **d == file_id) {
                    //panic!();
                    exists = FileState::Deleted;
                }
                exists
            },
        }
    }

    fn find_free_sheet(
        &self,
        file: FileId,
        mut page_buffer: &mut BlockDevice::Page,
    ) -> Result<SheetId, Error> {
        Ok(match self.state.root_position {
            None => SheetId(file, 0),
            Some(root_idx) => {
                let start_idx = match self.find_file_start(file) {
                    FileState::Present(block_idx) => block_idx,
                    _ => { return Ok(SheetId(file, 0)) },
                };
            dbg!(start_idx);
                let mut candidate = SheetId(file, 0);
                let mut traversal = LogTraversal::<_, C>::new(&self.device, root_idx);
                let mut started = false;

                while let Some((log_entry, entry_idx, entry_key))
                    = traversal.next(&mut page_buffer)
                {
                    dbg!(&log_entry, entry_idx);
                    started = started | (start_idx == entry_idx);
                    if dbg!(!started) {
                        continue;
                    }
                    // Sheet number is kept increasing as the log progresses.
                    // It's also impossible to remove individual sheets.
                    if log_entry.sheet == candidate {
                        let new_sheetno = candidate.1.checked_add(1);
                        candidate = match dbg!(new_sheetno) {
                            Some(s) => SheetId(file, s),
                            None => return Err(Error::NoSpace),
                        };
                    }
                    dbg!(candidate);
                }
                dbg!(candidate)
            }
        })
    }
    
    /// Reads a sheet from a file.
    ///
    /// To check which sheets a file contains,
    /// start this with `SheetId(file_id, 0)`,
    /// and scan consecutive sheet numbers.
    /// Stops if `f` returns `false`.
    ///
    /// If the sheet does not exist, `f` is not called.
    /// `f` may get called with arbitrary buffer sizes,
    /// including empty. But always in order.
    pub fn read_from_sheet<F: FnMut(&[u8]) -> bool>(
        &self,
        sheet_id: SheetId,
        f: &mut F,
    ) {
        // No point saving the key,
        // because traversal can't continue from the middle of a branch.
        let start_idx = match self.find_file_start(sheet_id.0) {
            FileState::Present(block_idx) => block_idx,
            _ => { return; },
        };

        match self.state.root_position {
            None => {},
            Some(root_idx) => {
                let mut page_buffer = BlockDevice::Page::uninitialized();
                let mut current_idx = root_idx;
                let mut current_key = C::Key::new_null();
                let mut started = false;
                // log traversal
                while let Some((log_entry, next_entry_idx, next_entry_key))
                    = Self::log_traverse(
                        &self.device,
                        &mut page_buffer,
                        current_idx,
                        current_key,
                    )
                {
                    dbg!(current_idx);
                                    dbg!(&log_entry);
                    started = started | (start_idx == current_idx);
                    if !started {
                        current_idx = next_entry_idx;
                        current_key = next_entry_key;
                        continue;
                    }
                    if dbg!(log_entry.sheet) == dbg!(sheet_id) {
                        let mut remaining_log_entry_bytes = log_entry.get_length();
                        let mut page_idx = PageIdx(current_idx, 0);
                        // data traversal within block
                        while page_idx.0 == current_idx {
                            let validity = Self::page_read(
                                &self.device,
                                &mut page_buffer,
                                page_idx,
                                current_key,
                            );
                            if let PageValidity::Invalid = validity {
                                break;
                            }
                            let buf = page_extract_payload::<C, _>(
                                &page_buffer,
                                remaining_log_entry_bytes,
                            );
                            if f(buf) == false {
                                return;
                            }
                            remaining_log_entry_bytes
                                = remaining_log_entry_bytes.saturating_sub(
                                    BlockDevice::Geometry::BYTES_IN_PAGE_COUNT - C::BYTES_COUNT
                                );
                            page_idx = page_idx.next();
                        }
                    }
                    current_idx = next_entry_idx;
                    current_key = next_entry_key;
                }
            }
        }
    }
}

struct LogTraversal<'a, BlockDevice: storage::Device, C: Checksum> {
    device: &'a BlockDevice,
    next_pos: BlockIdx<BlockDevice::Geometry>,
    next_key: C::Key,
}

impl<'a, BlockDevice: storage::Device, C: Checksum> LogTraversal<'a, BlockDevice, C> {
    fn new(
        device: &'a BlockDevice,
        root: BlockIdx<BlockDevice::Geometry>,
    ) -> Self {
        Self {
            device,
            next_pos: root,
            next_key: C::Key::new_null(),
        }
    }
    fn next(&mut self, buf: &mut BlockDevice::Page)
        -> Option<(
            LogEntry<BlockDevice::Geometry, C::Key>,
            BlockIdx<BlockDevice::Geometry>,
            C::Key
        )>
    {
        let (current_pos, current_key) = (self.next_pos, self.next_key);
        if let Some((entry, next_pos, next_key))
            = Seismos::<BlockDevice, C>::log_traverse(self.device, buf, current_pos, current_key)
        {
            self.next_pos = next_pos;
            self.next_key = next_key;
            Some((entry, current_pos, current_key))
        } else {
            None
        }
    }
}

#[cfg(feature="std")]
#[cfg(test)]
mod test {
    use super::*;
    use assert_matches::assert_matches;
    use crate::checksum::PrngKey;
    use crate::storage::ram::RAM;

    #[test]
    fn check_hash() {
        type DEV = RAM<4, 2, 8>;
        type FS = Seismos<DEV, checksum::Crc32>;
        let mut ram = DEV::new();
        let block = BlockIdx(2, Default::default());
        FS::_write_page_with_hash(&mut ram, PageIdx(block, 0), &[127; 256], PrngKey::new_null());
        let mut page_buffer = <DEV as storage::Device>::Page::uninitialized();
        assert_eq!(
            FS::find_root(&ram, &mut page_buffer),
            Some(block),
        );
        assert_eq!(&page_buffer.get_data()[..4], [127; 4].as_slice());
    }
    
    #[test]
    fn detect() {
        type DEV = RAM<4, 2, 32>;
        type FS = Seismos<DEV, checksum::Crc32>;
        let ram = DEV::new();
        let mut fs = FS::new(ram);
        assert_matches!(fs.force_commit_log(), Ok(()));
        let fs = FS::new(fs.device);
        assert_matches!(fs.state.root_position, Some(_));
    }

    // API tests
    #[test]
    fn outofspace() {
        type DEV = RAM<4, 2, 32>;
        type FS = Seismos<DEV, checksum::Crc32>;
        let ram = DEV::new();
        let mut fs = FS::new(ram);
        assert_matches!(fs.force_commit_log(), Ok(()));
        // root
        assert_matches!(fs.force_commit_log(), Ok(()));
        // root, 1
        assert_matches!(fs.force_commit_log(), Ok(()));
        // root, 1, 2
        assert_matches!(fs.force_commit_log(), Ok(()));
        // root, 1, 2, 3
        assert_matches!(fs.force_commit_log(), Err(Error::NoSpace));
    }
    
    #[test]
    fn fill_empty_cleanup() {
        type DEV = RAM<4, 2, 32>;
        type FS = Seismos<DEV, checksum::Crc32>;
        let ram = DEV::new();
        let mut fs = FS::new(ram);
        assert_matches!(fs.force_commit_log(), Ok(()));
        // root
        assert_matches!(fs.force_commit_log(), Ok(()));
        // root, 1
        assert_matches!(fs.force_commit_log(), Ok(()));
        // root, 1, 2
        assert_matches!(fs.force_commit_log(), Ok(()));
        // root, 1, 2, 3
        assert_matches!(fs.force_commit_log(), Err(Error::NoSpace));
        fs.collect_garbage();
        fs.force_commit_log().unwrap();
    }

    #[test]
    fn write_short() {
        type DEV = RAM<4, 2, 32>;
        type FS = Seismos<DEV, checksum::Crc32>;
        let ram = DEV::new();
        let mut fs = FS::new(ram);
        let mut sheet = FS::start_sheet(FileId(1));
        assert_matches!(
            fs.write_to_sheet(&mut sheet, &b"ABCD"[..]),
            Ok(())
        );
    }

    /// Checks if blocks are actually entered, based on useful capacity
    #[test]
    fn write_multi() {
        type DEV = RAM<2, 4, 32>; // should take 4*32 at half efficiency
        type FS = Seismos<DEV, checksum::Crc32>;
        let ram = DEV::new();
        let mut fs = FS::new(ram);
        let mut sheet = FS::start_sheet(FileId(1));

        for i in 0..32 {
            dbg!(i);
            fs.write_to_sheet(&mut sheet, &b"ABCD"[..]).unwrap();
        }
        if let Err((e, _sheet)) = fs.finish_sheet(sheet) {
            panic!("{:?}", e);
        }
        let mut expected = [0; 32*4];
        for (i, v) in expected.iter_mut().enumerate() {
            *v = b'A' + (i % 4) as u8;
        }
        read_sheet_compare(&fs, SheetId(FileId(1), 0), expected);
    }

    
    /// Overflows a single page
    #[test]
    fn write_pages() {
        type DEV = RAM<4, 2, 32>;
        type FS = Seismos<DEV, checksum::Crc32>;
        let ram = DEV::new();
        let mut fs = FS::new(ram);
        let mut sheet = FS::start_sheet(FileId(1));
        assert_matches!(
            fs.write_to_sheet(&mut sheet, &b"ABCD"[..]),
            Ok(())
        );
        assert_matches!(
            fs.write_to_sheet(&mut sheet, &[1; 32][..]),
            Ok(())
        );
        if let Err((e, _sheet)) = fs.finish_sheet(sheet) {
            panic!("{:?}", e);
        }
        let mut buffer = [0; 64];
        let mut offset = 0;
        fs.read_from_sheet(
            SheetId(FileId(1), 0),
            &mut |buf| {
                let read_count = serialize_into(&mut buffer[offset..], buf);
                if dbg!(read_count) < buf.len() {
                    panic!(
                        "overflow at {}, could accept {} of {}",
                        offset, read_count, buf.len()
                    );
                }
                offset += read_count;
                true
            },
        );
        assert!(offset >= 4 + 32, "Not everything read");
        assert_eq!(&buffer[..4], &b"ABCD"[..]);
        assert_eq!(&buffer[4..36], &[1;32][..]);
    }
        
    /// Overflows a single block, on finish
    #[test]
    fn write_blocks() {
        type DEV = RAM<4, 2, 32>;
        type FS = Seismos<DEV, checksum::Crc32>;
        let ram = DEV::new();
        let mut fs = FS::new(ram);
        write_sheet_at_once(&mut fs, FileId(1), &[1; 64]).unwrap();
        read_sheet_compare(&fs, SheetId(FileId(1), 0), [1; 64]);
    }
    
    /// Overflows a single block, on write
    #[test]
    fn write_loooonger() {
        type DEV = RAM<4, 2, 32>;
        type FS = Seismos<DEV, checksum::Crc32>;
        let ram = DEV::new();
        let mut fs = FS::new(ram);
        write_sheet_at_once(&mut fs, FileId(1), &[1; 128]).unwrap();
        read_sheet_compare(&fs, SheetId(FileId(1), 0), [1; 128]);
    }
    
    /// Overflows a single page in one write
    #[test]
    fn write_loong() {
        type DEV = RAM<4, 2, 32>;
        type FS = Seismos<DEV, checksum::Crc32>;
        let ram = DEV::new();
        let mut fs = FS::new(ram);
        let mut sheet = FS::start_sheet(FileId(1));
        assert_matches!(
            fs.write_to_sheet(&mut sheet, &[1; 32][..]),
            Ok(())
        );
        if let Err((e, _sheet)) = fs.finish_sheet(sheet) {
            panic!("{:?}", e);
        }
        let mut buffer = [0; 64];
        let mut offset = 0;
        fs.read_from_sheet(
            SheetId(FileId(1), 0),
            &mut |buf| {
                let read_count = serialize_into(&mut buffer[offset..], buf);
                if dbg!(read_count) < buf.len() {
                    panic!(
                        "overflow at {}, could accept {} of {}",
                        offset, read_count, buf.len()
                    );
                }
                offset += read_count;
                true
            },
        );

        assert!(offset >= 32, "Not everything read");
        assert_eq!(&buffer[..32], &[1;32][..]);
    }
    
    #[test]
    fn read_missing() {
        type DEV = RAM<4, 2, 32>;
        type FS = Seismos<DEV, checksum::Crc32>;
        let ram = DEV::new();
        let fs = FS::new(ram);

        fs.read_from_sheet(
            SheetId(FileId(1), 0),
            &mut |_| {
                panic!("Found file but it doesn't exist");
            },
        );
    }
    
    #[test]
    fn write_delete() {
        type DEV = RAM<4, 2, 32>;
        type FS = Seismos<DEV, checksum::Crc32>;
        let ram = DEV::new();
        let mut fs = FS::new(ram);
        let mut sheet = FS::start_sheet(FileId(1));
        assert_matches!(
            fs.write_to_sheet(&mut sheet, &[1; 32][..]),
            Ok(())
        );
        if let Err((e, _sheet)) = fs.finish_sheet(sheet) {
            panic!("{:?}", e);
        }
        
        fs.delete_file(FileId(1)).unwrap();
        
        fs.read_from_sheet(
            SheetId(FileId(1), 0),
            &mut |_| {
                panic!("Found file but it doesn't exist");
            },
        );
    }

    #[test]
    fn write_delete_write() {
        type DEV = RAM<4, 2, 32>;
        type FS = Seismos<DEV, checksum::Crc32>;
        let ram = DEV::new();
        let mut fs = FS::new(ram);
        let mut sheet = FS::start_sheet(FileId(1));
        assert_matches!(
            fs.write_to_sheet(&mut sheet, &[1; 8][..]),
            Ok(())
        );
        if let Err((e, _sheet)) = fs.finish_sheet(sheet) {
            panic!("{:?}", e);
        }
        
        fs.delete_file(FileId(1)).unwrap();

        let mut sheet = FS::start_sheet(FileId(1));
        fs.write_to_sheet(&mut sheet, &[2; 8][..]).unwrap();
        if let Err((e, _sheet)) = fs.finish_sheet(sheet) {
            panic!("{:?}", e);
        }
        read_sheet_compare(&fs, SheetId(FileId(1), 0), [2; 8]);
    }

    #[test]
    fn write_delete_reads() {
        type DEV = RAM<4, 2, 32>;
        type FS = Seismos<DEV, checksum::Crc32>;
        let ram = DEV::new();
        let mut fs = FS::new(ram);
        let mut sheet = FS::start_sheet(FileId(1));
        assert_matches!(
            fs.write_to_sheet(&mut sheet, &[1; 8][..]),
            Ok(())
        );
        if let Err((e, _sheet)) = fs.finish_sheet(sheet) {
            panic!("{:?}", e);
        }
        
        let mut sheet = FS::start_sheet(FileId(1));
        assert_matches!(
            fs.write_to_sheet(&mut sheet, &[2; 8][..]),
            Ok(())
        );
        if let Err((e, _sheet)) = fs.finish_sheet(sheet) {
            panic!("{:?}", e);
        }
        
        fs.delete_file(FileId(1)).unwrap();

        fs.read_from_sheet(
            SheetId(FileId(1), 0),
            &mut |_| {
                panic!("Found file but it doesn't exist");
            },
        );
        fs.read_from_sheet(
            SheetId(FileId(1), 1),
            &mut |_| {
                panic!("Found file but it doesn't exist");
            },
        );
    }
    
    
    #[test]
    fn writes_delete_writes_reads() {
        type DEV = RAM<4, 2, 32>;
        type FS = Seismos<DEV, checksum::Crc32>;
        let ram = DEV::new();
        let mut fs = FS::new(ram);
        let mut sheet = FS::start_sheet(FileId(1));
        assert_matches!(
            fs.write_to_sheet(&mut sheet, &[1; 8][..]),
            Ok(())
        );
        if let Err((e, _sheet)) = fs.finish_sheet(sheet) {
            panic!("{:?}", e);
        }
        
        let mut sheet = FS::start_sheet(FileId(1));
        assert_matches!(
            fs.write_to_sheet(&mut sheet, &[2; 8][..]),
            Ok(())
        );
        if let Err((e, _sheet)) = fs.finish_sheet(sheet) {
            panic!("{:?}", e);
        }
        
        fs.delete_file(FileId(1)).unwrap();

        let mut sheet = FS::start_sheet(FileId(1));
        assert_matches!(
            fs.write_to_sheet(&mut sheet, &[3; 8][..]),
            Ok(())
        );
        if let Err((e, _sheet)) = fs.finish_sheet(sheet) {
            panic!("{:?}", e);
        }
        
        read_sheet_compare(&fs, SheetId(FileId(1), 0), [3; 8]);
        
        fs.read_from_sheet(
            SheetId(FileId(1), 1),
            &mut |_| {
                panic!("Found file but it doesn't exist");
            },
        );
    }
    
    #[test]
    fn writes_to_full() {
        type DEV = RAM<4, 2, 32>;
        type FS = Seismos<DEV, checksum::Crc32>;
        let ram = DEV::new();
        let mut fs = FS::new(ram);

        // only 4 blocks = 4 slots for sheets of files
        write_sheet_at_once(&mut fs, FileId(1), &[1; 8]).unwrap();
        write_sheet_at_once(&mut fs, FileId(1), &[2; 8]).unwrap();
        write_sheet_at_once(&mut fs, FileId(1), &[3; 8]).unwrap();
        write_sheet_at_once(&mut fs, FileId(1), &[4; 8]).unwrap();

        assert_matches!(
            write_sheet_at_once(&mut fs, FileId(1), &[5; 8]),
            Err(Error::NoSpace)
        );
    }
    
    #[test]
    fn writes_to_full_read() {
        type DEV = RAM<4, 2, 32>;
        type FS = Seismos<DEV, checksum::Crc32>;
        let ram = DEV::new();
        let mut fs = FS::new(ram);

        // only 4 blocks = 4 slots for sheets of files
        write_sheet_at_once(&mut fs, FileId(1), &[1; 8]).unwrap();
        write_sheet_at_once(&mut fs, FileId(1), &[2; 8]).unwrap();
        write_sheet_at_once(&mut fs, FileId(1), &[3; 8]).unwrap();
        write_sheet_at_once(&mut fs, FileId(1), &[4; 8]).unwrap();

        read_sheet_compare(&fs, SheetId(FileId(1), 0), [1; 8]);
        read_sheet_compare(&fs, SheetId(FileId(1), 1), [2; 8]);
        read_sheet_compare(&fs, SheetId(FileId(1), 2), [3; 8]);
        read_sheet_compare(&fs, SheetId(FileId(1), 3), [4; 8]);
    }
    
    #[test]
    fn full_delete_write() {
        type DEV = RAM<4, 2, 32>;
        type FS = Seismos<DEV, checksum::Crc32>;
        let ram = DEV::new();
        let mut fs = FS::new(ram);

        // only 4 blocks = 4 slots for sheets of files
        write_sheet_at_once(&mut fs, FileId(1), &[1; 8]).unwrap();
        write_sheet_at_once(&mut fs, FileId(1), &[2; 8]).unwrap();
        write_sheet_at_once(&mut fs, FileId(1), &[3; 8]).unwrap();
        write_sheet_at_once(&mut fs, FileId(1), &[4; 8]).unwrap();

        fs.delete_file(FileId(1)).unwrap();

        write_sheet_at_once(&mut fs, FileId(1), &[5; 8]).unwrap();
        
        read_sheet_compare(&fs, SheetId(FileId(1), 0), [5; 8]);
        
    }
    
    
    fn write_sheet_at_once<D: storage::Device, C: Checksum>(
        fs: &mut Seismos<D, C>,
        file: FileId,
        contents: &[u8],
    ) -> Result<(), Error> {
        let mut sheet = Seismos::<D, C>::start_sheet(file);
        fs.write_to_sheet(&mut sheet, contents)?;
        fs.finish_sheet(sheet).map_err(|(e, _sheet)| e)
    }
    
    fn read_sheet_compare<D: storage::Device, C: Checksum, const L: usize>(
        fs: &Seismos<D, C>,
        sheet: SheetId,
        expected: [u8; L],
    ) {
        let mut buffer = [0; L];
        let mut offset = 0;
        let mut found = false;
        fs.read_from_sheet(
            sheet,
            &mut |buf| {
                let read_count = serialize_into(&mut buffer[offset..], buf);
                if read_count == 0 && buf.len() > 0{
                    panic!(
                        "overflow at {}, still {} left",
                        offset, buf.len(),
                    );
                }
                offset += read_count;
                found = true;
                true
            },
        );
        assert!(found);
        assert!(offset >= L, "Not everything read");
        assert_eq!(buffer, expected);
    }

    #[test]
    fn read_sheets() {
        type DEV = RAM<4, 2, 32>;
        type FS = Seismos<DEV, checksum::Crc32>;
        let ram = DEV::new();
        let mut fs = FS::new(ram);

        let mut sheet = FS::start_sheet(FileId(1));
        fs.write_to_sheet(&mut sheet, &[1; 8][..]).unwrap();
        if let Err((e, _sheet)) = fs.finish_sheet(sheet) {
            panic!("{:?}", e);
        }
        
        let mut sheet = FS::start_sheet(FileId(1));
        fs.write_to_sheet(&mut sheet, &[2; 8][..]).unwrap();
        if let Err((e, _sheet)) = fs.finish_sheet(sheet) {
            panic!("{:?}", e);
        }
        read_sheet_compare(&fs, SheetId(FileId(1), 0), [1; 8]);
        read_sheet_compare(&fs, SheetId(FileId(1), 1), [2; 8]);
        fs.read_from_sheet(
            SheetId(FileId(1), 2),
            &mut |_| {
                panic!("Found file but it doesn't exist");
            },
        );
    }
    
    // unit tests
    #[test]
    fn bh_serialize() {
        let mut buf = [0; 64];
        let bh = BlockHeader {
            log: BlockLog::new().with_file_deletion(FileId(7)).0,
            sheet: SheetId(FileId(5), 1),
        };
        bh.serialize(&mut buf);
        let bnew
            = BlockHeader::<(), PrngKey>::from_buffer(
                dbg!(&buf[..(bh.get_length())])
            ).unwrap();
        assert_eq!(bh, bnew);
    }
}
